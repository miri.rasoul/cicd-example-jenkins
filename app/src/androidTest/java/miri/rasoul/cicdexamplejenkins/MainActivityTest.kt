package miri.rasoul.cicdexamplejenkins


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.filters.LargeTest
import androidx.test.runner.AndroidJUnit4
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Rasoul Miri on 4/22/19.
 */

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest {

    @get:Rule
    var intentsTestRule = IntentsTestRule(MainActivity::class.java)

    private lateinit var textView: ViewInteraction
    private lateinit var button: ViewInteraction

    @Before
    fun before() {
        textView = onView(withId(R.id.textView))
        button = onView(withId(R.id.button))
    }

    @Test
    fun checkChangeTextAfterPressedButton() {
        button.perform(click())
        Thread.sleep(5000)
        textView.check(matches(withText("Change")))
    }

}